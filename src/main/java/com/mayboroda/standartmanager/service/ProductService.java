package com.mayboroda.standartmanager.service;


import com.mayboroda.standartmanager.model.Product;
import com.mayboroda.standartmanager.model.repository.ProductRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Named
@ApplicationScoped
public class ProductService {

    @Inject
    private ProductRepository repository;

    public List<Product> getProducts() {
        List<Product> result = new ArrayList<>();

        repository.findAll().forEach(standartType -> result.add(standartType));
        return result;
    }

    public void deleteProduct(Product product) {
        repository.delete(product);
    }

    public void saveProduct(Product product) {
        repository.save(product);
    }

}
