package com.mayboroda.standartmanager.service;


import com.mayboroda.standartmanager.model.Standart;
import com.mayboroda.standartmanager.model.StandartKeepingLog;
import com.mayboroda.standartmanager.model.repository.StandartKeepingLogRepository;
import com.mayboroda.standartmanager.model.repository.StandartRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Named
@ApplicationScoped
public class StandartService {

    @Inject
    private StandartRepository repository;

    @Inject
    private StandartKeepingLogRepository logRepository;

    public List<Standart> getStandarts() {
        List<Standart> result = new ArrayList<>();

        repository.findAll().forEach(standart -> result.add(standart));

        return result;
    }

    public Standart getStandart(Integer id) {
        return repository.findById(id).get();
    }

    public void deleteStandart(Standart object) {
        repository.delete(object);
    }

    public void saveStandart(Standart object) {
        repository.save(object);
    }

    public List<StandartKeepingLog> getLogsForStandart(Standart standart) {
        List<StandartKeepingLog> logs = new ArrayList<>();
        logRepository.findAll().forEach(l -> logs.add(l));

        if (logs == null || logs.size() == 0)
            return new ArrayList<>();

        return logs.stream().filter(l -> l.getStandarts().contains(standart)).collect(Collectors.toList());
    }

    public List<StandartKeepingLog> getNonReturnedLogs(Standart standart) {
        List<StandartKeepingLog> logs = getLogsForStandart(standart);

        if (logs == null || logs.size() == 0)
            return new ArrayList<>();

        return logs.stream().filter(l -> l.getDateEnd() == null).collect(Collectors.toList());
    }

    public String getStandartCopiesLocationStr(Standart standart) {
        StringBuilder places = new StringBuilder();

        getNonReturnedLogs(standart).forEach(log -> places.append(log.getLocation().getName().concat("\n<br />")));
        return places.toString();
    }

}
