package com.mayboroda.standartmanager.service;

import com.mayboroda.standartmanager.model.Standart;
import com.mayboroda.standartmanager.model.StandartKeepingLog;
import com.mayboroda.standartmanager.model.repository.StandartKeepingLogRepository;
import com.mayboroda.standartmanager.model.repository.StandartRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Named
@ApplicationScoped
public class StandartKeepingLogService {

    @Inject
    private StandartKeepingLogRepository repository;

    @Inject
    private StandartRepository standartRepository;

    public List<StandartKeepingLog> getStandartKeepingLogs() {
        List<StandartKeepingLog> result = new ArrayList<>();

        repository.findAll().forEach(log -> result.add(log));

        return result;
    }

    public void deleteStandartKeepingLog(StandartKeepingLog log) {
        repository.delete(log);
    }

    public void saveStandartKeepingLog(StandartKeepingLog log) {
        repository.save(log);
    }

    public List<Standart> getStandarts() {
        List<Standart> result = new ArrayList<>();
        standartRepository.findAll().forEach(s -> result.add(s));
        return result;
    }

}
