package com.mayboroda.standartmanager.service;


import com.mayboroda.standartmanager.model.Department;
import com.mayboroda.standartmanager.model.repository.DepartmentRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Named
@ApplicationScoped
public class DepartmentService {

    @Inject
    private DepartmentRepository departmentRepository;


    public List<Department> getDepartments() {
        List<Department> result = new ArrayList<>();

        departmentRepository.findAll().forEach(department -> result.add(department));
        return result;
    }

    public void deleteDepartment(Department department) {
        departmentRepository.delete(department);
    }

    public void updateDepartment(Department department) {
        departmentRepository.save(department);

    }

}
