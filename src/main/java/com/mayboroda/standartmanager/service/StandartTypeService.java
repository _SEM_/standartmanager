package com.mayboroda.standartmanager.service;

import com.mayboroda.standartmanager.model.StandartType;
import com.mayboroda.standartmanager.model.repository.StandartTypeRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Named
@ApplicationScoped
public class StandartTypeService {

    @Inject
    private StandartTypeRepository repository;

    public List<StandartType> getStandartTypes() {
        List<StandartType> result = new ArrayList<>();

        repository.findAll().forEach(standartType -> result.add(standartType));
        return result;
    }

    public void deleteStandartType(StandartType standartType) {
        repository.delete(standartType);
    }

    public void saveStandartRepository(StandartType standartType) {
        repository.save(standartType);
    }

}
