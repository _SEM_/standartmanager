package com.mayboroda.standartmanager.model.repository;

import com.mayboroda.standartmanager.model.StandartType;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface StandartTypeRepository extends CrudRepository<StandartType, Integer> {

    List<StandartType> findByCode(String code);
}
