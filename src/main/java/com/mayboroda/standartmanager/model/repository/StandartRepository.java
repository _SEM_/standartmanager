package com.mayboroda.standartmanager.model.repository;

import com.mayboroda.standartmanager.model.Standart;
import org.springframework.data.repository.CrudRepository;

public interface StandartRepository extends CrudRepository<Standart, Integer> {
}
