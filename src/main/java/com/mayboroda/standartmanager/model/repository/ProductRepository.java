package com.mayboroda.standartmanager.model.repository;

import com.mayboroda.standartmanager.model.Product;
import com.mayboroda.standartmanager.model.Standart;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProductRepository extends CrudRepository<Product, Integer> {

    List<Standart> findAllByStandarts_Id(Integer id);
}
