package com.mayboroda.standartmanager.model.repository;

import com.mayboroda.standartmanager.model.Department;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DepartmentRepository extends CrudRepository<Department, Integer> {

    List<Department> findByName(String name);
}
