package com.mayboroda.standartmanager.model.repository;

import com.mayboroda.standartmanager.model.StandartKeepingLog;
import org.springframework.data.repository.CrudRepository;

public interface StandartKeepingLogRepository extends CrudRepository<StandartKeepingLog, Integer> {
}
