package com.mayboroda.standartmanager.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * StandartType - тип стандарта, ГОСТ, ОСТ, ТУ и т.п.
 */

@Data
@Entity
@NoArgsConstructor
public class StandartType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String code;

    public StandartType(String code) {
        this.code = code;
    }
}
