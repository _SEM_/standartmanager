package com.mayboroda.standartmanager.model;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

/**
 * Standart - стандарт
 */

@Data
@Entity
@NoArgsConstructor
public class Standart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    private StandartType type;

    @OneToOne
    private Standart predecessor;

//    @ManyToMany(fetch = FetchType.EAGER)
//    private List<Product> products;

    private String regNum;

    private String name;

    private Boolean isActive = Boolean.TRUE;

    private Boolean hasElectronicCopy;

    private String pathToDocument;

    private LocalDate createDate = LocalDate.now();

    private String mainCopyLocation = "";

    public Standart(StandartType type, Standart predecessor,
                    //List<Product> products,
                    String regNum, String name, Boolean isActive, Boolean hasElectronicCopy, String pathToDocument) {
        this.type = type;
        this.predecessor = predecessor;
//        this.products = products;
        this.regNum = regNum;
        this.name = name;
        this.isActive = isActive;
        this.hasElectronicCopy = hasElectronicCopy;
        this.pathToDocument = pathToDocument;
    }
}
