package com.mayboroda.standartmanager.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Product - продукт, изделие
 */

@Data
@Entity
@NoArgsConstructor
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "product_standarts",
            joinColumns={ @JoinColumn(name="product_id", referencedColumnName="id", unique=false) },
            inverseJoinColumns={ @JoinColumn(name="standart_id", referencedColumnName="id", unique=false) })
    private List<Standart> standarts = new ArrayList<>();

    private String name;

    @Column(nullable = true)
    private String desciption = "";

    public Product(List<Standart> standarts, String name) {
        this.standarts = standarts;
        this.name = name;
    }
}
