package com.mayboroda.standartmanager.view;

import com.mayboroda.standartmanager.model.StandartType;
import com.mayboroda.standartmanager.service.StandartTypeService;
import lombok.Setter;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Setter
@Named
@ViewScoped
public class StandartTypeView {

    private StandartType selected = new StandartType();

    @Inject
    private StandartTypeService service;

    public List<StandartType> getStandartTypes() {
        return service.getStandartTypes();
    }


    public void delete(StandartType standartType) {
        service.deleteStandartType(standartType);
    }

    public void update() {
        service.saveStandartRepository(selected);
        selected = new StandartType();
    }

    public void prepareNew() {
        selected = new StandartType();
    }

    public StandartType getSelected() {
        return selected;
    }
}
