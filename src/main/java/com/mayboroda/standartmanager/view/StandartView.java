package com.mayboroda.standartmanager.view;

import com.mayboroda.standartmanager.model.Standart;
import com.mayboroda.standartmanager.model.StandartType;
import com.mayboroda.standartmanager.service.StandartService;
import com.mayboroda.standartmanager.service.StandartTypeService;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Named
@ViewScoped
public class StandartView {

    private static final Logger LOGGER = LoggerFactory.getLogger(StandartView.class);

    private static final String FILE_STORAGE_WEB = "/resources/data/";
    private static final String FILE_STORAGE_INTERNAL = "./src/main/webapp" + FILE_STORAGE_WEB;


    private Standart selected = new Standart();

    private StandartType selectedStandartType = new StandartType();

    private List<Standart> filteredPredecessors = new ArrayList<>();
    private List<Standart> filteredStandarts;

    private UploadedFile file;

    @Inject
    private StandartService service;

    @Inject
    private StandartTypeService standartTypeService;

    public void delete(Standart object) {
        service.deleteStandart(object);
    }

    public void update() {
        service.saveStandart(selected);
        selected = new Standart();
    }

    public void deleteElectronicCopy() {
        selected.setHasElectronicCopy(false);
        selected.setPathToDocument(null);
    }

    public void prepareNew() {
        selected = new Standart();
    }

    public List<StandartType> getStandartTypes() {
        return standartTypeService.getStandartTypes();
    }

    public void fileUploadListener(FileUploadEvent e) {
        this.file = e.getFile();
        File standScan = new File(FILE_STORAGE_INTERNAL + file.getFileName());

        try {
            OutputStream outputStream = new FileOutputStream(standScan);
            outputStream.write(file.getContents());
        } catch (Exception ex) {
            LOGGER.error("Error uploading file", ex);
        }
        selected.setPathToDocument(FILE_STORAGE_WEB + file.getFileName());
        selected.setHasElectronicCopy(Boolean.TRUE);

        LOGGER.info("file uploaded: {}", standScan.getAbsolutePath());
    }


    public Standart getSelected() {
        return selected;
    }

    public List<Standart> getStandarts() {
        return service.getStandarts();
    }

    public List<Standart> getPredecessors() {
        List<Standart> result = new ArrayList<>(service.getStandarts());
        result.remove(selected);
        return result;
    }

    public String getStandartLocation() {
        return service.getStandartCopiesLocationStr(selected);
    }
}
