package com.mayboroda.standartmanager.view;

import com.mayboroda.standartmanager.model.Department;
import com.mayboroda.standartmanager.model.Standart;
import com.mayboroda.standartmanager.model.StandartKeepingLog;
import com.mayboroda.standartmanager.service.DepartmentService;
import com.mayboroda.standartmanager.service.StandartKeepingLogService;
import lombok.Getter;
import lombok.Setter;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Getter
@Setter
@Named
@ViewScoped
public class StandartKeepingLogView {

    private StandartKeepingLog selected = new StandartKeepingLog();

    @Inject
    private StandartKeepingLogService service;

    @Inject
    private DepartmentService departmentService;

    private List<Standart> filteredLogStandarts;
    private List<StandartKeepingLog> filteredLogs;

    public List<StandartKeepingLog> getLogs() {
        return service.getStandartKeepingLogs();
    }

    public void update() {
        service.saveStandartKeepingLog(selected);
        selected = new StandartKeepingLog();
    }

    public void delete() {
        service.deleteStandartKeepingLog(selected);
    }

    public List<Standart> getStandarts() {
        return service.getStandarts();
    }

    public List<Department> getDepartments(){
        return departmentService.getDepartments();
    }

    public void prepareNew() {
        selected = new StandartKeepingLog();
    }

}
