package com.mayboroda.standartmanager.view;


import com.mayboroda.standartmanager.model.Department;
import com.mayboroda.standartmanager.service.DepartmentService;
import lombok.Setter;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Setter
@Named
@ViewScoped
public class DepartmentView implements Serializable {

    private Department selected = new Department();

    @Inject
    private DepartmentService service;

    public List<Department> getDepartments() {
        return service.getDepartments();
    }

    public void delete(Department department) {
        service.deleteDepartment(department);
    }

    public void update() {
        service.updateDepartment(selected);
        selected = new Department();
    }

    public void prepareNew() {
        selected = new Department();
    }

    public Department getSelected() {
        return selected;
    }

    public void setSelected(Department selected) {
        this.selected = selected;
    }
}
