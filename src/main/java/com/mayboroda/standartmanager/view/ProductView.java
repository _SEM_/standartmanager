package com.mayboroda.standartmanager.view;


import com.mayboroda.standartmanager.model.Product;
import com.mayboroda.standartmanager.model.Standart;
import com.mayboroda.standartmanager.service.ProductService;
import lombok.Getter;
import lombok.Setter;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Getter
@Setter
@Named
@ViewScoped
public class ProductView {

    private Product selected = new Product();

    @Inject
    private ProductService service;

    private List<Standart> filteredStandarts;

    public List<Product> getProducts() {
        return service.getProducts();
    }

    public void update() {
        service.saveProduct(selected);
        selected = new Product();
    }

    public Product getSelected() {
        return selected;
    }

}
