package com.mayboroda.standartmanager;

import com.mayboroda.standartmanager.model.*;
import com.mayboroda.standartmanager.model.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.LocalDate;
import java.util.*;

@SpringBootApplication
public class StandartmanagerApplication {

    private static final Logger log = LoggerFactory.getLogger(StandartmanagerApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(StandartmanagerApplication.class, args);
    }

    @Bean
    public CommandLineRunner demo(DepartmentRepository departmentRepository,
                                  ProductRepository productRepository,
                                  StandartRepository standartRepository,
                                  StandartTypeRepository standartTypeRepository,
                                  StandartKeepingLogRepository standartKeepingLogRepository) {
        return (args -> {
            List<StandartType> standType = new ArrayList<>();

            standartTypeRepository.findAll().forEach(st -> standType.add(st));

            if (standType.size()>0)
                return;

            Department department = new Department("Test");
            standartTypeRepository.save(new StandartType("ГОСТ"));
            StandartType standartType = new StandartType("ГОСТ Р");
            departmentRepository.save(department);
            standartTypeRepository.save(new StandartType("ГОСТ РВ"));
            standartTypeRepository.save(standartType);
            standartTypeRepository.save(new StandartType("ПС и Р"));
            standartTypeRepository.save(new StandartType("СП"));
            standartTypeRepository.save(new StandartType("СТО"));
            standartTypeRepository.save(new StandartType("ТУ"));
            standartTypeRepository.save(new StandartType("ISO"));

            Product test_p = new Product();

            test_p.setName("Изделие №2");

            productRepository.save(test_p);

            List<Standart> standart = Arrays.asList(new Standart(
                            standartTypeRepository.findByCode("ГОСТ").get(0),
                            null,
                            // Arrays.asList(productRepository.findById(1).get()),
                            "103-2006",
                            "Прокат сортовой стальной горячекатаный полосовой. Сортамент",
                            true,
                            true,
                            "/resources/data/gost-103-2006.pdf"),
                    new Standart(
                            standartTypeRepository.findByCode("ГОСТ").get(0),
                            null,
                            //  null,
                            "103-76",
                            "Полоса стальная горячекатаная. Сортамент (с Изменениями N 1, 2, 3)",
                            false,
                            false,
                            null));


            standart.stream().forEach(st -> standartRepository.save(st));

            final List<Standart> db_standart = new ArrayList<>();
            standartRepository.findAll().forEach(stt -> db_standart.add(stt));

            Product product = new Product(db_standart, "Изделие №1");

            productRepository.save(product);

            log.info("======================");

            departmentRepository.findAll().forEach(obj -> log.info(obj.toString()));
            standartTypeRepository.findAll().forEach(obj -> log.info(obj.toString()));
            standartRepository.findAll().forEach(obj -> log.info(obj.toString()));


            for (Product p1 : productRepository.findAll()) {
                log.info(Objects.toString(p1));
            }
            //productRepository.findAll().forEach(pp -> log.info(Objects.toString(pp)));


            departmentRepository.save(new Department("Отдел производства"));
            departmentRepository.save(new Department("Отдел контроля технологического процесса"));
            departmentRepository.save(new Department("Отдел отдел метрологии и качества"));

            Department logDep = departmentRepository.findByName("Отдел производства").get(0);

            Standart logStandart = standartRepository.findById(1).get();

            StandartKeepingLog log = new StandartKeepingLog();

            log.setDateStart(new Date());
            log.setLocation(logDep);
            log.setStandarts(Arrays.asList(logStandart));
            standartKeepingLogRepository.save(log);

        });
    }
}
